<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('pesel')->nullable();
            $table->string('nip')->nullable();
            $table->string('address');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('description');
            $table->string('hobby');
            $table->string('skills');
            $table->string('experience');
            $table->date('birthday_date');
            $table->date('register_date');
            $table->date('update_date');
            $table->int('name');
            $table->string('cv');
            $table->int('role_id'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
};
